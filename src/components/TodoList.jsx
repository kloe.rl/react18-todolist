import PropTypes from "prop-types";
import TodoItem from "./TodoItem";

function TodoList(props) {
  const { todos } = props;
  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>
        {/* creation d'un tableau (à partir du tableau 'todos') qui utilise le composant TodoItem et qui affiche le titre et l'état */}
        {todos.map(item => <TodoItem
          title={item.title} completed={item.completed} key={item.id}
        />)}
      </ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;